/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboles;

/**
 *
 * @author victhor_brito
 */
public class Arboles {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
   int dato;
int nombre;
ArbolBinario miArbol = new ArbolBinario();
miArbol.agregarNodo(86, "V" );
miArbol.agregarNodo(73, "I" );
miArbol.agregarNodo(67, "C" );
miArbol.agregarNodo(84, "T" );
miArbol.agregarNodo(79, "O" );
miArbol.agregarNodo(82, "R" );
miArbol.agregarNodo(77, "M" );
miArbol.agregarNodo(65, "A" );
miArbol.agregarNodo(78, "N" );
miArbol.agregarNodo(85, "U" );
miArbol.agregarNodo(69, "E" );
miArbol.agregarNodo(76, "L" );
miArbol.agregarNodo(80, "P" );
miArbol.agregarNodo(72, "H" );
miArbol.agregarNodo(66, "B" );


System.out.println("--------------inOrden");
if (!miArbol.estaVacio()){
miArbol.inOrden(miArbol.raiz);
}
    
System.out.println("--------------[Preorden]");
if (!miArbol.estaVacio()){
miArbol.preOrden(miArbol.raiz);
}
System.out.println("--------------[Posorden]");
if (!miArbol.estaVacio()){
miArbol.posOrden(miArbol.raiz);
}

    }
    }
    

